/**
 * The copyright in this software is being made available under the BSD License,
 * included below. This software may be subject to other third party and contributor
 * rights, including patent rights, and no such rights are granted under this license.
 *
 * Copyright (c) 2013, Dash Industry Forum.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation and/or
 *  other materials provided with the distribution.
 *  * Neither the name of Dash Industry Forum nor the names of its
 *  contributors may be used to endorse or promote products derived from this software
 *  without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */
import FactoryMaker from '../../core/FactoryMaker';

/**
 * @module XHRLoader
 * @ignore
 * @description Manages download of resources via HTTP.
 * @param {Object} cfg - dependencies from parent
 */
function XHRLoader(cfg) {

    cfg = cfg || {};
    const requestModifier = cfg.requestModifier;

    let instance;

    function load(httpRequest) {

        // Variables will be used in the callback functions
        const requestStartTime = new Date();
        const request = httpRequest.request;
        const range = request.range ? request.range.toString().split('-') : null;
        const iEnd = request.iEnd; //is NaN when unset
        /*rel_range = 0;
        unrel_range = 0;

        if (iEnd == range[0]) {
            rel_range = 0;
            unrel_range = request.range;
        }
        else if (iEnd == range[1]) {
            reliable_range   = request.range;
            unreliable_range = 0;
        }
        else {
        */
        //}
        //UGLY
        if (isNaN(iEnd)) {
            let xhr = new XMLHttpRequest();
            xhr.open(httpRequest.method, httpRequest.url, true);

            if (request.responseType) {
                xhr.responseType = request.responseType;
            }

            if (request.range) {
                xhr.setRequestHeader('Range', 'bytes=' + request.range);
            }

            if (!request.requestStartDate) {
                request.requestStartDate = requestStartTime;
            }

            if (requestModifier) {
                xhr = requestModifier.modifyRequestHeader(xhr);
            }

            xhr.withCredentials = httpRequest.withCredentials;

            xhr.onload = httpRequest.onload;
            xhr.onloadend = httpRequest.onend;
            xhr.onerror = httpRequest.onerror;
            xhr.onprogress = httpRequest.progress;
            xhr.onabort = httpRequest.onabort;

            xhr.send();

            httpRequest.response = xhr;
        }
        else {
            let rel_range   = range[0].toString() + '-' + iEnd.toString();
            let unrel_range = (parseInt(iEnd) + 1).toString() + '-' + range[1].toString();

            let xhr = new XMLHttpRequest();
            let first_half;
            xhr.open(httpRequest.method, httpRequest.url, true);


            xhr.setRequestHeader('Range', 'bytes=' + rel_range);
            xhr.setRequestHeader('x-slipstream-unreliable', 'false');

            if (request.responseType) {
                xhr.responseType = request.responseType;
            }

            if (!request.requestStartDate) {
                request.requestStartDate = requestStartTime;
            }

            if (requestModifier) {
                xhr = requestModifier.modifyRequestHeader(xhr);
            }

            xhr.withCredentials = httpRequest.withCredentials;
            xhr.onprogress = httpRequest.progress;

            xhr.onload = function () {
                first_half = xhr.response;
                console.log('first_half: ' + first_half.byteLength);

                let xhr2 = new XMLHttpRequest();
                xhr2.open(httpRequest.method, httpRequest.url, true);

                if (request.responseType) {
                    xhr2.responseType = request.responseType;
                }

                xhr2.setRequestHeader('Range', 'bytes=' + unrel_range);
                xhr2.setRequestHeader('x-slipstream-unreliable', 'true');

                if (!request.requestStartDate) {
                    request.requestStartDate = requestStartTime;
                }

                if (requestModifier) {
                    xhr2 = requestModifier.modifyRequestHeader(xhr2);
                }

                xhr2.withCredentials = httpRequest.withCredentials;

                xhr2.onerror = httpRequest.onerror;
                xhr2.onprogress = httpRequest.progress;
                xhr2.onabort = httpRequest.onabort;
                xhr2.onloadend = httpRequest.onend;

                xhr2.onload = function () {
                    httpRequest.first_half = first_half;
                    console.log('second_half: ' + xhr2.response.byteLength);
                    httpRequest.onload();
                };

                xhr2.send();

                httpRequest.response = xhr2;

            };

            xhr.send();

        }
    }

    function abort(request) {
        const x = request.response;
        x.onloadend = x.onerror = x.onprogress = undefined; //Ignore events from aborted requests.
        x.abort();
    }

    instance = {
        load: load,
        abort: abort
    };

    return instance;
}

XHRLoader.__dashjs_factory_name = 'XHRLoader';

const factory = FactoryMaker.getClassFactory(XHRLoader);
export default factory;
